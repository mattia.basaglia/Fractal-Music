/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "note.hpp"
#include <algorithm>
using namespace media::sound;


Note Scale::increase ( const Note& note )
{
    auto it = std::find(pitches.begin(),pitches.end(), note.pitch());
    if ( it == pitches.end() )
    {
        return note + 1;
    }
    ++it;
    int oct = 0;
    if ( it == pitches.end() )
    {
        it = pitches.begin();
        oct = 1;
    }
    return Note( *it, note.octave()+oct );
}

Note Scale::decrease ( const Note& note )
{
    auto it = std::find(pitches.begin(),pitches.end(), note.pitch());
    if ( it == pitches.end() )
    {
        return note - 1;
    }
    int oct = 0;
    if ( it == pitches.begin() )
    {
        it = pitches.end();
        oct = -1;
    }
    --it;
    return Note( *it, note.octave()+oct );
}

namespace media {
namespace sound {
namespace scale {
//{ Major
    Scale Db = { Note::C , Note::Cs, Note::Ds, Note::F , Note::Fs, Note::Gs, Note::As };
    Scale Ab = { Note::C , Note::Cs, Note::Ds, Note::F , Note::G , Note::Gs, Note::As };
    Scale Eb = { Note::C , Note::D , Note::Ds, Note::F , Note::G , Note::Gs, Note::As };
    Scale Bb = { Note::C , Note::D , Note::Ds, Note::F , Note::G , Note::A , Note::As };
    Scale F  = { Note::C , Note::D , Note::E , Note::F , Note::Fs, Note::A , Note::As };
    Scale C  = { Note::C , Note::D , Note::E , Note::F , Note::G , Note::A , Note::B  };
    Scale G  = { Note::C , Note::D , Note::E , Note::Fs, Note::G , Note::A , Note::B  };
    Scale D  = { Note::Cs, Note::D , Note::E , Note::Fs, Note::G , Note::A , Note::B  };
    Scale A  = { Note::Cs, Note::D , Note::E , Note::Fs, Note::Gs, Note::A , Note::B  };
    Scale E  = { Note::Cs, Note::Cs, Note::E , Note::Fs, Note::Gs, Note::A , Note::B  };
    Scale B  = { Note::Cs, Note::Cs, Note::E , Note::Fs, Note::Gs, Note::As, Note::B  };
    Scale Fs = { Note::Cs, Note::Cs, Note::F , Note::Fs, Note::Gs, Note::As, Note::B  };
//}
//{ Minor
    Scale Bbm= { Note::C , Note::Cs, Note::Ds, Note::F , Note::Fs, Note::Gs, Note::As };
    Scale Fm = { Note::C , Note::Cs, Note::Ds, Note::F , Note::G , Note::Gs, Note::As };
    Scale Cm = { Note::C , Note::D , Note::Ds, Note::F , Note::G , Note::Gs, Note::As };
    Scale Gm = { Note::C , Note::D , Note::Ds, Note::F , Note::G , Note::A , Note::As };
    Scale Dm = { Note::C , Note::D , Note::E , Note::F , Note::G , Note::A , Note::As };
    Scale Am = { Note::C , Note::D , Note::E , Note::F , Note::G , Note::A , Note::B  };
    Scale Em = { Note::C , Note::D , Note::E , Note::Fs, Note::G , Note::A , Note::B  };
    Scale Bm = { Note::Cs, Note::D , Note::E , Note::Fs, Note::G , Note::A , Note::B  };
    Scale Fsm= { Note::Cs, Note::D , Note::E , Note::Fs, Note::G , Note::A , Note::B  };
    Scale Csm= { Note::Cs, Note::Ds, Note::E , Note::Fs, Note::Gs, Note::A , Note::B  };
    Scale Gsm= { Note::Cs, Note::Ds, Note::E , Note::Fs, Note::Gs, Note::As, Note::B  };
    Scale Dsm= { Note::Cs, Note::Ds, Note::F , Note::Fs, Note::Gs, Note::As, Note::B  };
//}
}}}
