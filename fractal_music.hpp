/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef FRACTAL_MUSIC_HPP
#define FRACTAL_MUSIC_HPP

#include "sound.hpp"
#include "l-system.hpp"
#include <stack>

namespace media {
namespace sound {

/**
 * \brief Class that creates music from a L-System
 */
class Translator
{
public:
    /**
     * \brief Context used to generate the music
     */
    struct Context
    {
        Note   note = Note::C;  ///< Current note
        boost::rational<int>value{1,4};///< Note duration (fraction of a whole note)
        Scale  scale = scale::C;///< Scale used to increment the note
        std::stack<Note> stack; ///< Stacked notes
        Staff  output;          ///< Output music

        /// Increase note pitch
        void increase_pitch() { note = scale.increase(note); }
        /// Increase note pitch
        void decrease_pitch() { note = scale.decrease(note); }

        /// Increase note duration
        void increase_value() { value *= 2; }
        /// Increase note pitch
        void decrease_value() { value /= 2; }

        /// Push the current note into the stack
        void push() { stack.push(note); }
        /// Pop a note from the stack
        void pop()
        {
            if ( !stack.empty() )
            {
                note = stack.top();
                stack.pop();
            }
        }

        /// Write the current note to the output
        void write() { output.insert(note,value); }
        /// Write a rest to the output
        void rest() { output.insert(Note(),value); }
    };
    /**
     * \brief Translation rule functor
     */
    typedef std::function<void(Context&)> Rule;

    std::map<char,Rule> rules;  ///< Translation rules

    /**
     * \brief Translate a string into music
     */
    Staff translate(const std::string& input, Context ctx ) const;
    /**
     * \brief Translate a string into music
     */
    Staff translate(const std::string& input) const;
};

extern const Translator default_translator;



}} // media::sound
#endif // FRACTAL_MUSIC_HPP
