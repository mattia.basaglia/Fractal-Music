/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "fractal_music.hpp"

using namespace media::sound;

Staff Translator::translate(const std::string& input, Context ctx) const
{
    for(char c : input )
    {
        auto it = rules.find(c);
        if ( it != rules.end() )
            it->second(ctx);
    }
    return ctx.output;
}

Staff Translator::translate(const std::string& input) const { return translate(input,Context()); }

const Translator media::sound::default_translator {{
    {'+',[](Translator::Context&c){c.increase_pitch();}},
    {'-',[](Translator::Context&c){c.decrease_pitch();}},
    {'f',[](Translator::Context&c){c.write();}},
    {'a',[](Translator::Context&c){c.write();}},
    {'b',[](Translator::Context&c){c.write();}},
    {'F',[](Translator::Context&c){c.rest();}},
    {'A',[](Translator::Context&c){c.rest();}},
    {'B',[](Translator::Context&c){c.rest();}},
    {'[',[](Translator::Context&c){c.push();}},
    {']',[](Translator::Context&c){c.pop();}},
}};
