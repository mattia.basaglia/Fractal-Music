/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef SOUND_HPP
#define SOUND_HPP

#include <vector>
#include <cstdint>
#include <functional>
#include "music.hpp"

namespace media {
namespace sound {

/**
 * \brief Functor generating the wave
 * \param pos   Position within the sample [0,1)
 * \return A normalized sound in [-1,1]
 */
typedef std::function<double (double pos)> Waveform;

/**
 * \brief Normalize position within the wave
 * \param offset    Number of microseconds at the start of the sample to be generated
 * \param frequency Frequency of the wave to be generate in Hz
 * \param phase     Offset of the start of the wave
 * \return A value in [0,1)
 */
constexpr double sample_position(const Duration& offset, double frequency, double phase)
{
    return std::fmod( phase + double( Duration::rep(offset.count()*frequency) % second ) / second,  1.0 );
}

/**
 * \brief Represent a sampled sound wave
 * \todo function to change the sample rate
 * \todo check for sample_rate in operator+ and append
 */
struct Sampled_Wave
{
    typedef int16_t                         Sample;
    typedef std::vector<Sample>             container_type;
    typedef container_type::iterator        iterator;
    typedef container_type::const_iterator  const_iterator;

    static constexpr Sample crest = std::numeric_limits<Sample>::max();
    static constexpr Sample trough = std::numeric_limits<Sample>::min();

    container_type samples;
    unsigned       sample_rate;

    explicit Sampled_Wave(unsigned sample_rate = 44100)
     : sample_rate(sample_rate) {}


    iterator begin() { return samples.begin(); }
    iterator end() { return samples.end(); }
    const_iterator begin() const { return samples.begin(); }
    const_iterator end() const { return samples.end(); }
    const_iterator cbegin() const { return samples.begin(); }
    const_iterator cend() const { return samples.end(); }

    /**
     * \brief Add two waves together
     */
    Sampled_Wave operator+ ( const Sampled_Wave & o ) const
    {
        if ( o.samples.size() > samples.size() )
            return o + *this;
        Sampled_Wave result = *this;
        for ( unsigned i = 0; i < o.samples.size(); i++ )
            result.samples[i] += o.samples[i];
        return result;
    }

    /**
     * \brief Add two waves together
     */
    Sampled_Wave& operator+= ( const Sampled_Wave & o )
    {
        if ( o.samples.size() > samples.size() )
            samples.resize(o.samples.size());
        for ( unsigned i = 0; i < o.samples.size(); i++ )
            samples[i] += o.samples[i];
        return *this;
    }

    /**
     * \brief Multiply amplitude
     */
    Sampled_Wave operator* ( double factor ) const
    {
        Sampled_Wave result = *this;
        for(auto& s : result)
            s *= factor;
        return result;
    }

    /**
     * \brief Divide amplitude
     */
    Sampled_Wave operator/ ( double factor ) const
    {
        Sampled_Wave result = *this;
        for(auto& s : result)
            s /= factor;
        return result;
    }

    /**
     * \brief Append data from other into the wave
     */
    void append( const Sampled_Wave& other )
    {
        samples.insert(samples.end(),other.begin(),other.end());
    }


    /**
     * \brief Merge a number of waves together in a long stream
     */
    static Sampled_Wave chain(const std::vector<Sampled_Wave>& waves)
    {
        if ( waves.empty() )
            return Sampled_Wave();
        Sampled_Wave result(waves[0].sample_rate);
        for(const auto&w : waves)
            result.append(w);
        return result;
    }

    /**
     * \brief Add a number of waves together
     */
    static Sampled_Wave merge(const std::vector<Sampled_Wave>& waves)
    {
        if ( waves.empty() )
            return Sampled_Wave();
        Sampled_Wave result(waves[0].sample_rate);
        for(const auto&w : waves)
            result += w/waves.size();
        return result;
    }

    /**
     * \brief Generate a wave from a single note
     * \param note          Note defining the frequency
     * \param waveform      Shape of the wave
     * \param duration      Time for which the note needs to be sustained
     * \param phase         Wave starting phase
     * \param sample_rate   Rate of wave sampling in Hz
     */
    static Sampled_Wave generate(const Note& note,
                                 const Waveform& waveform,
                                 const Duration& duration,
                                 double phase = 0,
                                 unsigned sample_rate = 44100 );

    /**
     * \brief Generate a wave from a piece of music
     * \param music         Staff containing the notes
     * \param waveform      Shape of the wave
     * \param tempo         Tempo
     * \param sample_rate   Rate of wave sampling in Hz
     */
    static Sampled_Wave generate(const Staff& music,
                                 const Waveform& waveform,
                                 const Tempo& tempo,
                                 unsigned sample_rate = 44100 );

    /**
     * \brief Generate a wave from a piece of music
     * \param music         Music
     * \param waveform      Shape of the wave
     * \param sample_rate   Rate of wave sampling in Hz
     */
    static Sampled_Wave generate(const Music& music,
                                 const Waveform& waveform,
                                 unsigned sample_rate = 44100 );

private:

    /**
     * \brief Generate a wave from a single note
     * \param note          Note defining the frequency
     * \param waveform      Shape of the wave
     * \param duration      Time for which the note needs to be sustained
     * \param[in,out] phase Wave starting phase
     * \param sample_rate   Rate of wave sampling in Hz
     * \param[out] samples  Sample container
     */
    static void generate(const Note& note,
                         const Waveform& waveform,
                         const Duration& duration,
                         double& phase,
                         unsigned sample_rate,
                         container_type& samples );

};


}} // namespace media::sound
#endif // SOUND_HPP
