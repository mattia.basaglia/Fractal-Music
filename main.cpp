#include <SFML/Graphics.hpp>
#include <iostream>
#include "fractal_music.hpp"
#include <SFML/Audio.hpp>

using namespace media::sound;

double square_wave(double pos)
{
    return pos < 0.5 ? -1 : 1;
}

double sine_wave(double pos)
{
    return std::sin(2*M_PI*pos);
}

double sawtooth_wave(double pos)
{
    return 2*pos - 1;
}


double weird_wave(double pos)
{
    return sine_wave(pos)*sine_wave(2*pos)*sawtooth_wave(pos);
}

double fm_sine_wave(double pos,double beta=1,double speed=2)
{
    return std::sin(2*M_PI*pos+beta*std::sin(2*M_PI*pos*speed));
}


double test_wave(double pos)
{
    return std::sin(2*M_PI*pos)/2 + std::sin(2*M_PI*pos*pos)/2;
}

/**
 * \brief Generate music from a fractal
 * \param fractal               Fractal used to generate the string
 * \param starting_iteration    Number of fractal iterations for the starting string
 * \param iterations            Number of fractal music iterations
 * \param starting_duration     Note value for the first music iteration (multiplier of a whole note)
 * \param wave                  Waveform functor
 * \param tempo                 Tempo used for the output
 * \param translator            Translator used to transform the fractal string into music
 * \param generator             Sampled wave generator
 */
Sampled_Wave generate_music(
    const misc::L_System& fractal,
    int                   starting_iteration,
    int                   iterations,
    boost::rational<int>  starting_duration,
    const Waveform&       waveform,
    const Tempo&          tempo,
    const Translator&     translator = default_translator,
    unsigned              sample_rate = 44100
)
{

    std::string result = fractal.apply_rules("f",starting_iteration);
    Music music;
    music.tempo = tempo;
    for ( int i = 0; i < iterations; i++ )
    {
        if ( i )
            result = fractal.apply_rules(result);
        Translator::Context ctx;
        ctx.value = starting_duration;
        starting_duration /= fractal.growth_factor();
        music.insert(translator.translate(result,ctx));
    }
    return Sampled_Wave::generate(music,waveform,sample_rate);
}
int main()
{
    using namespace std::placeholders;

    /*for ( int i = 0; i < 5; i++ )
    {
        Note n ( Note::C, i );
        for ( int j = 0; j < 7; j++ )
        {
            std::cout << n.name() << '\t';
            n = scale::Am.increase(n);
        }
        std::cout << "\n";
    }*/



    misc::L_System weed = {
        {'f',"ff+[+f-f-f]-[-f+f+f]"},
        {'f',"ff-[-f-f-f]+[+f+f+f]"},
        {'f',"ff+[f+f-f]-[f-f+f]"},
    };

    sf::SoundBuffer buff;
    Sampled_Wave wave = generate_music(weed,1,3,4,test_wave/*std::bind(fm_sine_wave,_1,0.5,3)*/,Tempo(120));
    buff.loadFromSamples(wave.samples.data(),wave.samples.size(),1,wave.sample_rate);
    buff.saveToFile("out.wav");
    sf::Sound sound(buff);
    sound.play();

    std::cin.get();

    return 0;
}
