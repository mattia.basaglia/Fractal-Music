/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef L_SYSTEM_HPP
#define L_SYSTEM_HPP

#include <string>
#include <map>
#include <algorithm>
namespace misc {

/**
 * \brief A set of L-System rules
 *
 * It allows stochastic rules (with equal probability).
 * Rules are context free and match a single character in input.
 */
class L_System
{
private:
    typedef std::multimap<char,std::string> Container;
    Container rules;

public:
    L_System( const Container& rules ) : rules(rules) {}
    L_System( const std::initializer_list<Container::value_type>& init ) : rules(init) {}

    /**
     * \brief Add a replacement rule for the given character
     */
    void add_rule ( char head, const std::string &production )
    {
        rules.insert ( std::make_pair(head,production) );
    }

    /**
     * \brief Remove all rules for a given character
     */
    void remove_rules ( char head )
    {
        rules.erase ( head );
    }

    /**
     * \brief Get (one of) the production(s) for the given character
     *
     * The identity profuction is used if there's no explicit rule
     */
    std::string get ( char head ) const
    {
        auto pair = rules.equal_range(head);
        if ( pair.first == pair.second )
            return std::string(1,head);
        int n = 0;
        for ( auto it = pair.first; it != pair.second; ++it )
            ++n;
        std::advance(pair.first,rand()%n); /// \todo use misc::math once (if) merged in the game
        return pair.first->second;
    }

    /**
     * \brief Apply rules to the given string \c n number of times
     */
    std::string apply_rules ( std::string input, int n = 1 ) const
    {
        for ( int i = 0; i < n; i++ )
        {
            std::string result;
            for ( auto ch : input )
                result += get(ch);
            input = result;
        }

        return input;
    }

    /**
     * \brief Get an approximation of the exponential growth of an input string
     */
    int growth_factor() const
    {
        std::string heads;
        for(const auto& pair : rules)
            if ( heads.empty() || heads.back() != pair.first )
                heads += pair.first;
        int max = 0;
        for(const auto& pair : rules)
        {
            int n = std::count_if(pair.second.begin(),pair.second.end(),
                [heads](char c){ return std::binary_search(heads.begin(),heads.end(),c); });
            if ( n > max )
                max = n;
        }
        return max;
    }
};


} // namespace misc
#endif // L_SYSTEM_HPP
