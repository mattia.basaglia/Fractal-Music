/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "sound.hpp"

using namespace media::sound;


void Sampled_Wave::generate(const Note& note,
                            const Waveform& waveform,
                            const Duration& duration,
                            double& phase,
                            unsigned sample_rate,
                            container_type& samples )
{
    Duration timer = Duration::zero();
    unsigned pos = samples.size();
    samples.resize(samples.size()+std::round(duration.count()*(double(sample_rate)/second)));

    if ( !note.is_rest() )
    {
        for ( ; pos < samples.size(); pos++ )
        {
            samples[pos] = waveform(sample_position(timer,note.frequency(),phase)) * crest;
            timer += std::chrono::microseconds( second / sample_rate );
        }
        phase = sample_position(timer,note.frequency(),phase);
    }
    else
        phase = 0;

}

Sampled_Wave Sampled_Wave::generate(const Note& note,
                                    const Waveform& waveform,
                                    const Duration& duration,
                                    double phase,
                                    unsigned sample_rate )
{
    Sampled_Wave wave(sample_rate);
    generate(note,waveform,duration,phase,sample_rate,wave.samples);
    return wave;
}

Sampled_Wave Sampled_Wave::generate(const Staff& music,
                                    const Waveform& waveform,
                                    const Tempo& tempo,
                                    unsigned sample_rate )
{
    Sampled_Wave wave(sample_rate);
    double phase = 0;
    for(const auto& pair : music )
        generate(pair.first,waveform,tempo.value(pair.second),phase,sample_rate,wave.samples);
    return wave;
}

Sampled_Wave Sampled_Wave::generate(const Music& music,
                                    const Waveform& waveform,
                                    unsigned sample_rate )
{
    std::vector<Sampled_Wave> waves;
    for ( const auto& staff : music )
        waves.push_back(generate(staff,waveform,music.tempo,sample_rate));
    return Sampled_Wave::merge(waves);
}
