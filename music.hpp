/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef MUSIC_HPP
#define MUSIC_HPP
#include <chrono>
#include "note.hpp"
#include <boost/rational.hpp>

namespace media {
namespace sound {
/**
 * \brief Represent the duration of a note
 */
typedef std::chrono::microseconds Duration;

/// Duration of one second
constexpr Duration::rep second = Duration::period::den / Duration::period::num;

/**
 * \brief Get a duration in seconds
 */
constexpr Duration seconds(double seconds)
{
    return Duration(Duration::rep(second * seconds));
}

/**
 * \brief Class representing a tempo
 */
class Tempo
{
private:
    Duration whole_time;

public:
    /**
     * \brief Create a tempo
     * \param bpm           Beats per minute, number of reference notes in a minute
     * \param note_fraction Reference note fraction (eg: 4 = quarter note)
     */
    constexpr explicit Tempo ( long bpm = 120, double note_fraction = 4 )
        : whole_time(seconds(60/(bpm/note_fraction))) {}

    constexpr Duration double_whole() const { return 2*whole_time; }
    constexpr Duration whole() const { return whole_time; }
    constexpr Duration half() const { return whole_time/2; }
    constexpr Duration quarter() const { return whole_time/4; }
    constexpr Duration eigth() const { return whole_time/8; }
    constexpr Duration sixteenth() const { return whole_time/16; }

    /**
     * \brief Get arbitrary note value
     * \param multiplier    Whole note multiplier
     */
    constexpr Duration value(double multiplier) const { return Duration(Duration::rep(whole_time.count()*multiplier)); }

    /**
     * \brief Get arbitrary note value
     * \param ratio         Ratio of a whole note
     */
    Duration value(const boost::rational<int>& ratio) const
    {
        return Duration(whole_time.count()*ratio.numerator()/ratio.denominator());
    }
};

/**
 * \brief A stream of single notes
 */
class Staff
{
public:
    typedef std::pair<Note,boost::rational<int>> value_type;
    typedef std::vector<value_type>         container;
    typedef container::iterator             iterator;
    typedef container::const_iterator       const_iterator;

private:
    container notes;

public:
    Staff() = default;
    Staff(const std::initializer_list<value_type>& notes) : notes(notes) {}


    iterator begin() { return notes.begin(); }
    iterator end() { return notes.end(); }
    const_iterator begin() const { return notes.begin(); }
    const_iterator end() const { return notes.end(); }
    const_iterator cbegin() const { return notes.begin(); }
    const_iterator cend() const { return notes.end(); }

    void insert(const Note& note, boost::rational<int> value = {1,4} )
    {
        notes.push_back({note,value});
    }
};

/**
 * \brief A piece of music
 */
class Music
{
public:
    typedef std::vector<Staff>          container;
    typedef container::value_type       value_type;
    typedef container::iterator         iterator;
    typedef container::const_iterator   const_iterator;

    container staffs;
    Tempo     tempo;

    Music() = default;
    Music(const std::initializer_list<value_type>& staffs) : staffs(staffs) {}

    iterator begin() { return staffs.begin(); }
    iterator end() { return staffs.end(); }
    const_iterator begin() const { return staffs.begin(); }
    const_iterator end() const { return staffs.end(); }
    const_iterator cbegin() const { return staffs.begin(); }
    const_iterator cend() const { return staffs.end(); }

    void insert(const value_type& staff)
    {
        staffs.push_back(staff);
    }
};

}} // media::sound
#endif // MUSIC_HPP
