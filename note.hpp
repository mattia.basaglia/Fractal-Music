/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef NOTE_HPP
#define NOTE_HPP

#include <sstream>
#include <cmath>
#include <vector>

namespace media {
namespace sound {
/**
 * \brief Represents a musical note
 */
class Note
{
public:
    /**
     * \brief Note names
     * \note The values indicate the distance from the A
     */
    enum Pitch { C = -9, Cs, D, Ds, E, F, Fs, G, Gs, A, As, B };
private:
    static constexpr double base_fequency  = 27.5; ///< Frequency of A0
    static constexpr double multiplier_delta = std::pow(2.,1./12.); ///< Difference between two consecutive frequencies

    double m_frequency;

    explicit constexpr Note ( double Hz ) : m_frequency(Hz) {}

    /**
     * \brief Get the exponent of base_fequency to get frequency
     */
    constexpr int exponent() const
    {
        return std::round(std::log(m_frequency/base_fequency) / std::log(multiplier_delta) );
    }
public:
    constexpr Note ( Pitch pitch, int octave = 4 )
     : m_frequency( base_fequency*std::pow(2,octave)*std::pow(multiplier_delta,pitch) )
    {}

    constexpr Note() : m_frequency(-1) {}

    /// Get the note frequency
    constexpr double frequency() const { return m_frequency; }

    /// Whether the note represents a rest
    constexpr bool is_rest() const { return m_frequency < 0; }

    /// Octave number
    constexpr int octave() const
    {
        return (exponent() - C) / 12;
    }

    /// Note pitch
    constexpr Pitch pitch() const
    {
        return Pitch ( (exponent() - C) % 12 + C );
    }

    /**
     * \brief Represent the note using the scientific pitch notation
     * \note Rests are represented by a dash
     */
    std::string name() const
    {
        if ( is_rest() )
            return "-";
        std::ostringstream ss;
        switch ( pitch() )
        {
            case C  : ss << "C"; break;
            case Cs : ss << "C#";break;
            case D  : ss << "D"; break;
            case Ds : ss << "D#";break;
            case E  : ss << "E"; break;
            case F  : ss << "F"; break;
            case Fs : ss << "F#";break;
            case G  : ss << "G"; break;
            case Gs : ss << "G#";break;
            case A  : ss << "A"; break;
            case As : ss << "A#";break;
            case B  : ss << "B"; break;
        }
        ss << octave();
        return ss.str();
    }

    /**
     * \brief Increment by \c n notes
     */
    constexpr Note operator+ ( int n ) const
    {
        return Note(m_frequency < 0 ? -1 : m_frequency * std::pow(multiplier_delta,n));
    }
    /**
     * \brief Decrement by \c n notes
     */
    constexpr Note operator- ( int n ) const
    {
        return *this + -n;
    }

    /**
     * \brief Increment by \c n notes
     */
    Note& operator+= ( int n )
    {
        m_frequency *= std::pow(multiplier_delta,n);
        return *this;
    }
    /**
     * \brief Decrement by \c n notes
     */
    Note& operator- ( int n )
    {
        return *this += -n;
    }

    /**
     * \brief Increment by one tone
     */
    Note& operator++ ()
    {
        if ( m_frequency > 0 )
            m_frequency *= multiplier_delta;
        return *this;
    }
    /**
     * \brief Decrement by one tone
     */
    Note& operator-- ()
    {
        if ( m_frequency > 0 )
            m_frequency /= multiplier_delta;
        return *this;
    }


    /**
     * \brief Increment by one tone
     */
    Note operator++ (int)
    {
        Note n = *this;
        ++*this;
        return n;
    }
    /**
     * \brief Decrement by one tone
     */
    Note operator-- (int)
    {
        Note n = *this;
        --*this;
        return n;
    }
};

/**
 * \brief A musical scale
 */
class Scale
{
private:
    std::vector<Note::Pitch> pitches;

public:
    Scale ( const std::initializer_list<Note::Pitch>& pitches ) : pitches ( pitches ) {}
    Scale() = default;

    /**
     * \brief Increase the note keeping it within the scale
     */
    Note increase ( const Note& note );

    /**
     * \brief Decrease the note keeping it within the scale
     */
    Note decrease ( const Note& note );
};

/**
 * \brief Scale preset
 */
namespace scale {
//{ Major
    extern Scale Db;
    extern Scale Ab;
    extern Scale Eb;
    extern Scale Bb;
    extern Scale F;
    extern Scale C;
    extern Scale G;
    extern Scale D;
    extern Scale A;
    extern Scale E;
    extern Scale B;
    extern Scale Fs;
//}
//{ Minor
    extern Scale Bbm;
    extern Scale Fm ;
    extern Scale Cm ;
    extern Scale Gm ;
    extern Scale Dm ;
    extern Scale Am ;
    extern Scale Em ;
    extern Scale Bm ;
    extern Scale Fsm;
    extern Scale Csm;
    extern Scale Gsm;
    extern Scale Dsm;
//}
};


}} // namespace media::sound
#endif // NOTE_HPP
